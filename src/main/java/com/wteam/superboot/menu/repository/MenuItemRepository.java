package com.wteam.superboot.menu.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.menu.entity.po.MenuItemPo;

public interface MenuItemRepository extends SuperRepository<MenuItemPo, Long> {

}
