package com.wteam.superboot.menu.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.menu.entity.po.MenuPo;

public interface MenuRepository extends SuperRepository<MenuPo, Long> {

}
