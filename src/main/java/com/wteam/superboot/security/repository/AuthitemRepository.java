package com.wteam.superboot.security.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.security.entity.po.AuthitemPo;

public interface AuthitemRepository extends SuperRepository<AuthitemPo, Long> {

}
