package com.wteam.superboot.security.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.security.entity.po.ActionPo;

public interface ActionRepository extends SuperRepository<ActionPo, Long> {

}
