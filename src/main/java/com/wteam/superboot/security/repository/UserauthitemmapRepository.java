package com.wteam.superboot.security.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.security.entity.po.UserauthitemmapPo;

public interface UserauthitemmapRepository extends SuperRepository<UserauthitemmapPo, Long> {

}
