package com.wteam.superboot.security.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.security.entity.po.ResourcePo;

public interface ResourceRepository extends SuperRepository<ResourcePo, Long> {

}
