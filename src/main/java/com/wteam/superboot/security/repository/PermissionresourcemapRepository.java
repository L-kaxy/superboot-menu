package com.wteam.superboot.security.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.security.entity.po.PermissionresourcemapPo;

public interface PermissionresourcemapRepository extends SuperRepository<PermissionresourcemapPo, Long> {

}
