package com.wteam.superboot.security.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.security.entity.po.ResourcetypePo;

public interface ResourcetypeRepository extends SuperRepository<ResourcetypePo, Long> {

}
