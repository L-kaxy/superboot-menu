package com.wteam.superboot.security.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.security.entity.po.UserkeyPo;

public interface UserkeyRepository extends SuperRepository<UserkeyPo, Long> {

}
