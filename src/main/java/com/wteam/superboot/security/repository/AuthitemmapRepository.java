package com.wteam.superboot.security.repository;

import com.wteam.superboot.core.repository.SuperRepository;
import com.wteam.superboot.security.entity.po.AuthitemmapPo;

public interface AuthitemmapRepository extends SuperRepository<AuthitemmapPo, Long> {

}
