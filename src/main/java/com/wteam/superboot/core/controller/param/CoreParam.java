/**
 * Copyright (c) 2007-2017 Wteam.  All rights reserved. 网维网络技术创业团队 版权所有.
 * 请勿修改或删除版权声明及文件头部.
 */
package com.wteam.superboot.core.controller.param;

/**
 * 返回参数.
 * 
 * @authod 罗佳欣
 * 
 */
public abstract class CoreParam {

}
