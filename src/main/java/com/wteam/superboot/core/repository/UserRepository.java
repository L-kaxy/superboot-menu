package com.wteam.superboot.core.repository;

import com.wteam.superboot.core.entity.po.UserPo;

public interface UserRepository extends SuperRepository<UserPo, Long> {

}
