package com.wteam.superboot.core.repository;

import com.wteam.superboot.core.entity.po.SystemconfigPo;

public interface SystemconfigRepository extends SuperRepository<SystemconfigPo, Long> {

}
