/**
 * Copyright (c) 2007-2017 Wteam.  All rights reserved. 网维网络技术创业团队 版权所有.
 * 请勿修改或删除版权声明及文件头部.
 */
package com.wteam.superboot.core.enums;

/**
 * 结果消息枚举接口.
 * 
 * @authod 罗佳欣
 * 
 */
public interface Resultinfo {

	public Integer getServiceResult();

	public String getResultInfo();
}
